package ru.t1.malyugin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.model.Project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return DBConstant.TABLE_PROJECT;
    }

    @NotNull
    @Override
    public Project fetch(@NotNull final ResultSet row) throws Exception {
        @NotNull final Project project = new Project();
        project.setId(row.getString(DBConstant.COLUMN_ROW_ID));
        project.setName(row.getString(DBConstant.COLUMN_NAME));
        project.setDescription(row.getString(DBConstant.COLUMN_DESCRIPTION));
        project.setStatus(Status.valueOf(row.getString(DBConstant.COLUMN_STATUS)));
        project.setCreated(row.getTimestamp(DBConstant.COLUMN_CREATED));
        project.setUserId(row.getString(DBConstant.COLUMN_USER_ID));
        return project;
    }

    @NotNull
    @Override
    public Project add(@NotNull final Project project) throws Exception {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?)",
                getTableName(), DBConstant.COLUMN_ROW_ID, DBConstant.COLUMN_USER_ID, DBConstant.COLUMN_CREATED,
                DBConstant.COLUMN_NAME, DBConstant.COLUMN_DESCRIPTION, DBConstant.COLUMN_STATUS
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getId());
            statement.setString(2, project.getUserId());
            statement.setTimestamp(3, new Timestamp(project.getCreated().getTime()));
            statement.setString(4, project.getName());
            statement.setString(5, project.getDescription());
            statement.setString(6, project.getStatus().toString());
            statement.executeUpdate();
        }
        return project;
    }

    @NotNull
    @Override
    public Project add(@NotNull final String userId, @NotNull final Project project) throws Exception {
        project.setUserId(userId);
        return add(project);
    }

    @Override
    public void update(@NotNull final Project project) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstant.COLUMN_NAME, DBConstant.COLUMN_DESCRIPTION, DBConstant.COLUMN_STATUS, DBConstant.COLUMN_ROW_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getName());
            statement.setString(2, project.getDescription());
            statement.setString(3, project.getStatus().toString());
            statement.setString(4, project.getId());
            statement.executeUpdate();
        }
    }

}