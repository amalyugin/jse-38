package ru.t1.malyugin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Session;
import ru.t1.malyugin.tm.model.User;

import static ru.t1.malyugin.tm.TestData.*;

@Category(UnitCategory.class)
public class SessionRepositoryTest {
    @NotNull
    private static User user1 = new User();

    @NotNull
    private static User user2 = new User();

    @BeforeClass
    public static void initUser() throws Exception {
        user1 = USER_SERVICE.create(FIRST_USUAL_USER_LOGIN, FIRST_USUAL_USER_PASS, null, Role.USUAL);
        user2 = USER_SERVICE.create(SECOND_USUAL_USER_LOGIN, SECOND_USUAL_USER_PASS, null, Role.USUAL);
    }

    @AfterClass
    public static void clearUser() throws Exception {
        USER_SERVICE.remove(user1);
        USER_SERVICE.remove(user2);
    }

    @Before
    public void initRepository() throws Exception {
        for (int i = 1; i <= NUMBER_OF_SESSION; i++) {
            @NotNull final Session session = new Session();
            if (i <= NUMBER_OF_SESSION / 2) session.setUserId(user1.getId());
            else session.setUserId(user2.getId());
            SESSION_REPOSITORY.add(session);
            SESSION_LIST.add(session);
        }
    }

    @After
    public void clearData() throws Exception {
        SESSION_REPOSITORY.clear();
        TASK_SERVICE.clear();
        PROJECT_SERVICE.clear();
        TASK_LIST.clear();
        PROJECT_LIST.clear();
        SESSION_LIST.clear();
    }

    @Test
    public void testAddSession() throws Exception {
        final int initialNumberOfEntries = SESSION_REPOSITORY.getSize();
        final int expectedNumberOfEntries = initialNumberOfEntries + 1;
        @NotNull final Session session = new Session();
        session.setRole(Role.USUAL);
        SESSION_LIST.add(session);
        SESSION_REPOSITORY.add(session);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize());
    }

    @Test
    public void testAddSessionForUser() throws Exception {
        final int initialNumberOfEntries = SESSION_REPOSITORY.getSize();
        final int expectedNumberOfEntries = initialNumberOfEntries + 1;
        @NotNull final Session session = new Session();
        SESSION_LIST.add(session);
        SESSION_REPOSITORY.add(user1.getId(), session);
        Assert.assertEquals(expectedNumberOfEntries, SESSION_REPOSITORY.getSize());
        @Nullable final Session actualSession = SESSION_REPOSITORY.findOneById(user1.getId(), session.getId());
        Assert.assertNotNull(actualSession);
        Assert.assertEquals(session.getId(), actualSession.getId());
    }

}