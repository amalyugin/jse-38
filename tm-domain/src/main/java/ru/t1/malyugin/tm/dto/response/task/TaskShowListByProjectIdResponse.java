package ru.t1.malyugin.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.response.AbstractResponse;
import ru.t1.malyugin.tm.model.Task;

import java.util.List;


@Getter
@Setter
@NoArgsConstructor
public class TaskShowListByProjectIdResponse extends AbstractResponse {

    @Nullable
    private List<Task> taskList;

    public TaskShowListByProjectIdResponse(@Nullable final List<Task> taskList) {
        this.taskList = taskList;
    }

}