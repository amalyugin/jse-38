package ru.t1.malyugin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.request.project.ProjectShowByIndexRequest;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-show-by-index";

    @NotNull
    private static final String DESCRIPTION = "Show project by index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");

        System.out.print("ENTER PROJECT INDEX: ");
        @NotNull final Integer index = TerminalUtil.nextInteger();

        @NotNull final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(getToken(), index);
        @Nullable final Project project = getProjectEndpoint().showProjectByIndex(request).getProject();
        renderProject(project);
    }

}