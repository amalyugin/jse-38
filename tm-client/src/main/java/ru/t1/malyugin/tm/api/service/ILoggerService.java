package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.Nullable;

import javax.xml.ws.soap.SOAPFaultException;

public interface ILoggerService {

    void info(@Nullable String message);

    void command(@Nullable String message);

    void debug(@Nullable String message);

    void error(@Nullable Exception e);

    void errorSOAP(@Nullable SOAPFaultException e);

}