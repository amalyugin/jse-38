package ru.t1.malyugin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    void update(@NotNull Project project) throws Exception;

}