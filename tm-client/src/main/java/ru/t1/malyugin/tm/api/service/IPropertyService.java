package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends IConnectionProvider {

    @NotNull
    String getApplicationName();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorName();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getApplicationConfig();

    @NotNull
    String getApplicationLogDir();

    @NotNull
    String getApplicationCommandScannerDir();

    @NotNull
    String getTestAdminLogin();

    @NotNull
    String getTestAdminPassword();

    @NotNull
    String getTestSoapLogin();

    @NotNull
    String getTestSoapPass();

}