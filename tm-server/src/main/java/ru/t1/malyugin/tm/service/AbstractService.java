package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.IRepository;
import ru.t1.malyugin.tm.api.service.IConnectionService;
import ru.t1.malyugin.tm.api.service.IService;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.exception.entity.EntityNotFoundException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.IndexIncorrectException;
import ru.t1.malyugin.tm.model.AbstractModel;

import java.sql.Connection;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected Connection getConnection() {
        return connectionService.getConnection(false);
    }

    @NotNull
    protected abstract IRepository<M> getRepository(@NotNull final Connection connection);

    @Nullable
    @Override
    public M add(@Nullable final M model) throws Exception {
        if (model == null) return null;
        @NotNull final Connection connection = getConnection();
        @NotNull final IRepository<M> repository = getRepository(connection);
        try {
            repository.add(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @NotNull
    @Override
    public Collection<M> addAll(@Nullable final Collection<M> models) throws Exception {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final IRepository<M> repository = getRepository(connection);
        try {
            repository.addAll(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return models;
    }

    @NotNull
    @Override
    public Collection<M> set(@Nullable final Collection<M> models) throws Exception {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        @NotNull final Connection connection = getConnection();
        @NotNull final IRepository<M> repository = getRepository(connection);
        try {
            repository.set(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return models;
    }

    @Override
    public void clear() throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final IRepository<M> repository = getRepository(connection);
        try {
            repository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public M remove(@Nullable final M model) throws Exception {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final Connection connection = getConnection();
        @NotNull final IRepository<M> repository = getRepository(connection);
        try {
            repository.remove(model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @Override
    public void removeAll(@Nullable final List<M> models) throws Exception {
        if (models == null || models.isEmpty()) return;
        @NotNull final Connection connection = getConnection();
        @NotNull final IRepository<M> repository = getRepository(connection);
        try {
            repository.removeAll(models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public M removeById(@Nullable final String id) throws Exception {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final M model = findOneById(id);
        if (model == null) throw new EntityNotFoundException();
        return remove(model);
    }

    @NotNull
    @Override
    public M removeByIndex(@Nullable final Integer index) throws Exception {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        @Nullable final M model = findOneByIndex(index);
        if (model == null) throw new EntityNotFoundException();
        return remove(model);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) throws Exception {
        if (StringUtils.isBlank(id)) return null;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findOneById(id.trim());
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) throws Exception {
        if (index == null || index <= 0 || index > getSize()) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findOneByIndex(index);
        }
    }

    @NotNull
    @Override
    public List<M> findAll() throws Exception {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll();
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws Exception {
        if (comparator == null) return findAll();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.findAll(comparator);
        }
    }

    @NotNull
    @SuppressWarnings("unchecked")
    @Override
    public List<M> findAll(@Nullable final Sort sort) throws Exception {
        if (sort == null) return findAll();
        @NotNull final Comparator<M> comparator = (Comparator<M>) sort.getComparator();
        return findAll(comparator);
    }

    @Override
    public int getSize() throws Exception {
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IRepository<M> repository = getRepository(connection);
            return repository.getSize();
        }
    }

}