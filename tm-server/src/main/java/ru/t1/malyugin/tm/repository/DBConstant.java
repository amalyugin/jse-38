package ru.t1.malyugin.tm.repository;

public interface DBConstant {

    String TABLE_PROJECT = "tm.TM_PROJECT";

    String TABLE_TASK = "tm.TM_TASK";

    String TABLE_SESSION = "tm.TM_SESSION";

    String TABLE_USER = "tm.TM_USER";

    String COLUMN_CREATED = "CREATED";

    String COLUMN_STATUS = "STATUS";

    String COLUMN_NAME = "NAME";

    String COLUMN_DESCRIPTION = "DESCRIPTION";

    String COLUMN_USER_ID = "USER_ID";

    String COLUMN_PROJECT_ID = "PROJECT_ID";

    String COLUMN_ROW_ID = "ROW_ID";

    String COLUMN_LOGIN = "LOGIN";

    String COLUMN_PASSWORD_HASH = "PASSWORD";

    String COLUMN_EMAIL = "EMAIL";

    String COLUMN_FIRST_NAME = "FIRST_NAME";

    String COLUMN_MIDDLE_NAME = "MIDDLE_NAME";

    String COLUMN_LAST_NAME = "LAST_NAME";

    String COLUMN_ROLE = "ROLE";

    String COLUMN_LOCKED = "LOCKED_FLG";

}