package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M> {

    @Nullable
    M add(
            @Nullable String userId,
            M model
    ) throws Exception;

    void clear(
            @Nullable String userId
    ) throws Exception;

    @NotNull
    M remove(
            @Nullable String userId,
            @Nullable M model
    ) throws Exception;

    void removeAll(
            @Nullable String userId,
            @Nullable List<M> models
    ) throws Exception;

    @NotNull
    M removeById(
            @Nullable String userId,
            @Nullable String id
    ) throws Exception;

    @NotNull
    M removeByIndex(
            @Nullable String userId,
            @Nullable Integer index
    ) throws Exception;

    @Nullable
    M findOneById(
            @Nullable String userId,
            @Nullable String id
    ) throws Exception;

    @Nullable
    M findOneByIndex(
            @Nullable String userId,
            @Nullable Integer index
    ) throws Exception;

    @NotNull
    List<M> findAll(
            @Nullable String userId
    ) throws Exception;

    @NotNull
    List<M> findAll(
            @Nullable String userId,
            @Nullable Sort sort
    ) throws Exception;

    @NotNull
    List<M> findAll(
            @Nullable String userId,
            @Nullable Comparator<M> comparator
    ) throws Exception;

    int getSize(
            @Nullable String userId
    ) throws Exception;

}