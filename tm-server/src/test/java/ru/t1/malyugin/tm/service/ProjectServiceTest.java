package ru.t1.malyugin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.entity.EntityNotFoundException;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.field.*;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.model.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static ru.t1.malyugin.tm.TestData.*;

@Category(UnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    private static User user1 = new User();

    @NotNull
    private static User user2 = new User();

    @BeforeClass
    public static void initUser() throws Exception {
        user1 = USER_SERVICE.create(FIRST_USUAL_USER_LOGIN, FIRST_USUAL_USER_PASS, null, Role.USUAL);
        user2 = USER_SERVICE.create(SECOND_USUAL_USER_LOGIN, SECOND_USUAL_USER_PASS, null, Role.USUAL);
    }

    @AfterClass
    public static void clearUser() throws Exception {
        USER_SERVICE.remove(user1);
        USER_SERVICE.remove(user2);
    }

    @Before
    public void initTest() throws Exception {
        for (int i = 1; i <= NUMBER_OF_PROJECTS; i++) {
            @NotNull final Project project = new Project("P" + i, "D" + i);
            if (i <= NUMBER_OF_PROJECTS / 2) project.setUserId(user1.getId());
            else project.setUserId(user2.getId());
            PROJECT_REPOSITORY.add(project);
            PROJECT_LIST.add(project);
        }
    }

    @After
    public void clearData() throws Exception {
        TASK_SERVICE.clear();
        PROJECT_SERVICE.clear();
        TASK_LIST.clear();
        PROJECT_LIST.clear();
    }

    @Test
    public void testAdd() throws Exception {
        Assert.assertNull(PROJECT_SERVICE.add(null));
        Assert.assertEquals(NUMBER_OF_PROJECTS, PROJECT_SERVICE.getSize());
        final int expectedNumberOfEntries = NUMBER_OF_PROJECTS + 1;
        @NotNull final Project project = new Project("NEW", "NEW");
        PROJECT_LIST.add(project);
        Assert.assertNotNull(PROJECT_SERVICE.add(project));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testAddForUser() throws Exception {
        Assert.assertNull(PROJECT_SERVICE.add(user1.getId(), null));
        final int expectedNumberOfEntries = NUMBER_OF_PROJECTS + 1;
        @NotNull final Project project = new Project("NEW", "NEW");
        project.setUserId(user1.getId());
        PROJECT_LIST.add(project);
        PROJECT_SERVICE.add(user1.getId(), project);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testAddForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.add(null, new Project()));
    }

    @Test
    public void testClearForUser() throws Exception {
        PROJECT_SERVICE.clear(UNKNOWN_ID);
        Assert.assertEquals(NUMBER_OF_PROJECTS, PROJECT_SERVICE.getSize());

        final int expectedNumberOfEntries = 0;
        PROJECT_SERVICE.clear(user1.getId());
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize(user1.getId()));
        Assert.assertNotEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize(user2.getId()));
    }

    @Test
    public void testClearForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.clear(null));
    }

    @Test
    public void testAddAll() throws Exception {
        Assert.assertEquals(Collections.emptyList(), PROJECT_SERVICE.addAll(null));
        final int expectedNumberOfEntries = NUMBER_OF_PROJECTS * 2;
        @NotNull final List<Project> ProjectListToAdd = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_PROJECTS; i++) {
            @NotNull final Project project = new Project("NEW", "NEW");
            PROJECT_LIST.add(project);
            ProjectListToAdd.add(project);
        }
        Assert.assertNotNull(PROJECT_SERVICE.addAll(ProjectListToAdd));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testSet() throws Exception {
        Assert.assertEquals(Collections.emptyList(), PROJECT_SERVICE.set(null));
        final int expectedNumberOfEntries = NUMBER_OF_PROJECTS + 1;
        @NotNull final List<Project> ProjectListToAdd = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_PROJECTS + 1; i++) {
            @NotNull final Project project = new Project("NEW", "NEW");
            PROJECT_LIST.add(project);
            ProjectListToAdd.add(project);
        }
        Assert.assertNotNull(PROJECT_SERVICE.set(ProjectListToAdd));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testClear() throws Exception {
        final int expectedNumberOfEntries = 0;
        PROJECT_SERVICE.clear();
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testRemove() throws Exception {
        final int expectedNumberOfEntries = NUMBER_OF_PROJECTS - 1;
        @NotNull final Project project = PROJECT_LIST.get(0);
        Assert.assertNotNull(PROJECT_SERVICE.remove(project));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testRemoveNegative() {
        Assert.assertThrows(EntityNotFoundException.class, () -> PROJECT_SERVICE.remove(null));
    }

    @Test
    public void testRemoveForUser() throws Exception {
        final int expectedNumberOfEntries = NUMBER_OF_PROJECTS - 1;
        @NotNull final Project project = PROJECT_LIST.get(0);
        PROJECT_SERVICE.remove(user2.getId(), project);
        Assert.assertEquals(NUMBER_OF_PROJECTS, PROJECT_SERVICE.getSize());
        PROJECT_SERVICE.remove(user1.getId(), project);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testRemoveForUserNegative() {
        Assert.assertThrows(EntityNotFoundException.class, () -> PROJECT_SERVICE.remove(user1.getId(), null));
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.remove(null, new Project()));
    }

    @Test
    public void testRemoveAll() throws Exception {
        final int expectedNumberOfEntries = NUMBER_OF_PROJECTS - 2;
        @NotNull final List<Project> projectListToRemove = new ArrayList<>();
        projectListToRemove.add(PROJECT_SERVICE.findAll().get(0));
        projectListToRemove.add(PROJECT_SERVICE.findAll().get(1));
        PROJECT_SERVICE.removeAll(null);
        Assert.assertEquals(NUMBER_OF_PROJECTS, PROJECT_SERVICE.getSize());
        PROJECT_SERVICE.removeAll(projectListToRemove);
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
        for (Project project : projectListToRemove) Assert.assertNull(PROJECT_SERVICE.findOneById(project.getId()));
    }

    @Test
    public void testRemoveById() throws Exception {
        final int expectedNumberOfEntries = NUMBER_OF_PROJECTS - 1;
        @NotNull final Project project = PROJECT_LIST.get(0);
        Assert.assertNotNull(PROJECT_SERVICE.removeById(project.getId()));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIdNegative() {
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.removeById(null));
        Assert.assertThrows(EntityNotFoundException.class, () -> PROJECT_SERVICE.removeById(UNKNOWN_ID));
    }

    @Test
    public void testRemoveByIdForUser() throws Exception {
        final int expectedNumberOfEntries = NUMBER_OF_PROJECTS - 1;
        @NotNull final Project project = PROJECT_SERVICE.findOneByIndex(1);
        Assert.assertNotNull(project);
        Assert.assertNotNull(PROJECT_SERVICE.removeById(user1.getId(), project.getId()));
        Assert.assertNull(PROJECT_SERVICE.findOneById(project.getId()));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIdForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.removeById(null, UNKNOWN_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_SERVICE.removeById(UNKNOWN_ID, null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> PROJECT_SERVICE.removeById(UNKNOWN_ID, UNKNOWN_ID));
    }

    @Test
    public void testRemoveByIndex() throws Exception {
        final int expectedNumberOfEntries = NUMBER_OF_PROJECTS - 1;
        @NotNull final Project project = PROJECT_SERVICE.findAll().get(0);
        Assert.assertNotNull(PROJECT_SERVICE.removeByIndex(1));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(project.getId()));
    }

    @Test
    public void testRemoveByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.removeByIndex(null));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.removeByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.removeByIndex(NUMBER_OF_PROJECTS + 1));
    }

    @Test
    public void testRemoveByIndexForUser() throws Exception {
        final int expectedNumberOfEntries = NUMBER_OF_PROJECTS - 1;
        @NotNull final Project project = PROJECT_SERVICE.findAll(user1.getId()).get(0);
        Assert.assertNotNull(PROJECT_SERVICE.removeByIndex(user1.getId(), 1));
        Assert.assertNull(PROJECT_SERVICE.findOneById(project.getId()));
        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIndexForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.removeByIndex(null, 1));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.removeByIndex(UNKNOWN_ID, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.removeByIndex(UNKNOWN_ID, -1));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.removeByIndex(UNKNOWN_ID, NUMBER_OF_PROJECTS + 1));
    }

    @Test
    public void testFindOneById() throws Exception {
        Assert.assertNull(PROJECT_SERVICE.findOneById(UNKNOWN_ID));
        Assert.assertNull(PROJECT_SERVICE.findOneById(null));
        for (@NotNull final Project project : PROJECT_LIST)
            Assert.assertEquals(project.getId(), PROJECT_SERVICE.findOneById(project.getId()).getId());
    }

    @Test
    public void testFindOneByIdForUser() throws Exception {
        Assert.assertNull(PROJECT_SERVICE.findOneById(null));
        Assert.assertNull(PROJECT_SERVICE.findOneById(UNKNOWN_ID));
        for (@NotNull final Project project : PROJECT_SERVICE.findAll(user1.getId()))
            Assert.assertEquals(project.getId(), PROJECT_SERVICE.findOneById(user1.getId(), project.getId()).getId());
        Assert.assertNull(PROJECT_SERVICE.findOneById(user1.getId(), null));
    }

    @Test
    public void testFindOneByIdForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.findOneById(null, UNKNOWN_ID));
    }

    @Test
    public void testFindOneByIndex() throws Exception {
        for (int i = 0; i < PROJECT_SERVICE.getSize(); i++) {
            Assert.assertEquals(PROJECT_LIST.get(i).getId(), PROJECT_SERVICE.findOneByIndex(i + 1).getId());
        }
    }

    @Test
    public void testFindOneByIndexNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.findOneByIndex(null));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.findOneByIndex(-1));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.findOneByIndex(NUMBER_OF_PROJECTS + 1));
    }

    @Test
    public void testFindOneByIndexForUser() throws Exception {
        @NotNull final List<Project> expectedProjectList = PROJECT_LIST.stream().filter(p -> user1.getId().equals(p.getUserId())).collect(Collectors.toList());

        for (int i = 0; i < PROJECT_SERVICE.getSize(user1.getId()); i++)
            Assert.assertEquals(expectedProjectList.get(i).getId(), PROJECT_SERVICE.findOneByIndex(user1.getId(), i + 1).getId());
    }

    @Test
    public void testFindOneByIndexForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.findOneByIndex(null, 1));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.findOneByIndex(UNKNOWN_ID, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.findOneByIndex(UNKNOWN_ID, -1));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.findOneByIndex(UNKNOWN_ID, NUMBER_OF_PROJECTS + 1));
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final List<Project> actualProjectList = PROJECT_SERVICE.findAll();
        Assert.assertEquals(PROJECT_LIST.size(), actualProjectList.size());
        for (int i = 0; i < PROJECT_LIST.size(); i++)
            Assert.assertEquals(PROJECT_LIST.get(i).getId(), actualProjectList.get(i).getId());
    }

    @Test
    public void testFindAllForUser() throws Exception {
        @NotNull final List<Project> actualProjectList = PROJECT_SERVICE.findAll(user1.getId());
        @NotNull final List<Project> expectedProjectList = PROJECT_LIST
                .stream()
                .filter(p -> user1.getId().equals(p.getUserId()))
                .collect(Collectors.toList());
        Assert.assertEquals(expectedProjectList.size(), actualProjectList.size());
        for (int i = 0; i < expectedProjectList.size(); i++) {
            Assert.assertEquals(expectedProjectList.get(i).getId(), actualProjectList.get(i).getId());
        }
    }

    @Test
    public void testFindAllForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.findAll((String) null));
    }

    @Test
    public void testFindAllWithSort() throws Exception {
        @NotNull final List<Project> actualProjectList = PROJECT_SERVICE.findAll();

        PROJECT_SERVICE.changeProjectStatusById(actualProjectList.get(0).getUserId(), actualProjectList.get(0).getId(), Status.IN_PROGRESS);
        PROJECT_SERVICE.changeProjectStatusById(actualProjectList.get(4).getUserId(), actualProjectList.get(4).getId(), Status.COMPLETED);

        Assert.assertEquals(Status.COMPLETED, PROJECT_SERVICE.findAll(Sort.BY_STATUS).get(0).getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, PROJECT_SERVICE.findAll(Sort.BY_STATUS).get(1).getStatus());

        PROJECT_SERVICE.updateById(actualProjectList.get(3).getUserId(), actualProjectList.get(3).getId(), "A", null);
        PROJECT_SERVICE.updateById(actualProjectList.get(1).getUserId(), actualProjectList.get(1).getId(), "B", null);

        Assert.assertEquals("A", PROJECT_SERVICE.findAll(Sort.BY_NAME).get(0).getName());
        Assert.assertEquals("B", PROJECT_SERVICE.findAll(Sort.BY_NAME).get(1).getName());

        @NotNull final List<Project> expectedProjectList = PROJECT_SERVICE.findAll();
        Assert.assertEquals(expectedProjectList.size(), PROJECT_SERVICE.findAll((Sort) null).size());
    }

    @Test
    public void testFindAllWithSortForUser() throws Exception {
        @NotNull final List<Project> actualProjectList = PROJECT_SERVICE.findAll(user1.getId());

        PROJECT_SERVICE.changeProjectStatusById(user1.getId(), actualProjectList.get(0).getId(), Status.IN_PROGRESS);
        PROJECT_SERVICE.changeProjectStatusById(user1.getId(), actualProjectList.get(4).getId(), Status.COMPLETED);

        Assert.assertEquals(Status.COMPLETED, PROJECT_SERVICE.findAll(user1.getId(), Sort.BY_STATUS).get(0).getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, PROJECT_SERVICE.findAll(user1.getId(), Sort.BY_STATUS).get(1).getStatus());

        PROJECT_SERVICE.updateById(user1.getId(), actualProjectList.get(3).getId(), "A", null);
        PROJECT_SERVICE.updateById(user1.getId(), actualProjectList.get(1).getId(), "B", null);

        Assert.assertEquals("A", PROJECT_SERVICE.findAll(user1.getId(), Sort.BY_NAME).get(0).getName());
        Assert.assertEquals("B", PROJECT_SERVICE.findAll(user1.getId(), Sort.BY_NAME).get(1).getName());

        @NotNull final List<Project> expectedProjectList = PROJECT_SERVICE.findAll(user1.getId());
        Assert.assertEquals(expectedProjectList.size(), PROJECT_SERVICE.findAll(user1.getId(), (Sort) null).size());
    }

    @Test
    public void testFindAllWithSortForUserForUser() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.findAll(null, Sort.BY_CREATED));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testFindAllWithComparator() throws Exception {
        @NotNull final List<Project> actualProjectList = PROJECT_SERVICE.findAll();
        @Nullable final Comparator<Project> comparatorByName = (Comparator<Project>) Sort.BY_NAME.getComparator();
        @Nullable final Comparator<Project> comparatorByStatus = (Comparator<Project>) Sort.BY_STATUS.getComparator();

        PROJECT_SERVICE.changeProjectStatusById(actualProjectList.get(0).getUserId(), actualProjectList.get(0).getId(), Status.IN_PROGRESS);
        PROJECT_SERVICE.changeProjectStatusById(actualProjectList.get(4).getUserId(), actualProjectList.get(4).getId(), Status.COMPLETED);

        Assert.assertEquals(Status.COMPLETED, PROJECT_SERVICE.findAll(comparatorByStatus).get(0).getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, PROJECT_SERVICE.findAll(comparatorByStatus).get(1).getStatus());


        PROJECT_SERVICE.updateById(actualProjectList.get(3).getUserId(), actualProjectList.get(3).getId(), "A", null);
        PROJECT_SERVICE.updateById(actualProjectList.get(1).getUserId(), actualProjectList.get(1).getId(), "B", null);
        Assert.assertEquals("A", PROJECT_SERVICE.findAll(comparatorByName).get(0).getName());
        Assert.assertEquals("B", PROJECT_SERVICE.findAll(comparatorByName).get(1).getName());

        @NotNull final List<Project> expectedProjectList = PROJECT_SERVICE.findAll();
        Assert.assertEquals(expectedProjectList.size(), PROJECT_SERVICE.findAll((Comparator<Project>) null).size());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testFindAllWithComparatorForUser() throws Exception {
        @NotNull final List<Project> actualProjectList = PROJECT_SERVICE.findAll(user1.getId());
        @Nullable final Comparator<Project> comparatorByName = (Comparator<Project>) Sort.BY_NAME.getComparator();
        @Nullable final Comparator<Project> comparatorByStatus = (Comparator<Project>) Sort.BY_STATUS.getComparator();

        PROJECT_SERVICE.changeProjectStatusById(user1.getId(), actualProjectList.get(0).getId(), Status.IN_PROGRESS);
        PROJECT_SERVICE.changeProjectStatusById(user1.getId(), actualProjectList.get(4).getId(), Status.COMPLETED);

        Assert.assertEquals(Status.COMPLETED, PROJECT_SERVICE.findAll(user1.getId(), comparatorByStatus).get(0).getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, PROJECT_SERVICE.findAll(user1.getId(), comparatorByStatus).get(1).getStatus());

        PROJECT_SERVICE.updateById(user1.getId(), actualProjectList.get(3).getId(), "A", null);
        PROJECT_SERVICE.updateById(user1.getId(), actualProjectList.get(1).getId(), "B", null);

        Assert.assertEquals("A", PROJECT_SERVICE.findAll(user1.getId(), comparatorByName).get(0).getName());
        Assert.assertEquals("B", PROJECT_SERVICE.findAll(user1.getId(), comparatorByName).get(1).getName());

        @NotNull final List<Project> expectedProjectList = PROJECT_SERVICE.findAll(user1.getId());
        Assert.assertEquals(expectedProjectList.size(), PROJECT_SERVICE.findAll(user1.getId(), (Comparator<Project>) null).size());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testFindAllWithComparatorForUserNegative() {
        @Nullable final Comparator<Project> comparatorByCreated = (Comparator<Project>) Sort.BY_CREATED.getComparator();
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.findAll(null, comparatorByCreated));
    }

    @Test
    public void testGetSize() throws Exception {
        Assert.assertEquals(PROJECT_LIST.size(), PROJECT_SERVICE.getSize());
    }

    @Test
    public void testGetSizeForUser() throws Exception {
        final int actualProjectList = PROJECT_SERVICE.getSize(user1.getId());
        Assert.assertEquals(actualProjectList, PROJECT_SERVICE.getSize(user1.getId()));
        Assert.assertEquals(0, PROJECT_SERVICE.getSize(UNKNOWN_ID));
    }

    @Test
    public void testGetSizeForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.getSize(null));
    }

    @Test
    public void testCrateProject() throws Exception {
        final int expectedNumberOfEntries = NUMBER_OF_PROJECTS + 2;
        @NotNull final Project project1 = PROJECT_SERVICE.create(user1.getId(), "NAME1", "DESCRIPTION");
        @NotNull final Project project2 = PROJECT_SERVICE.create(user2.getId(), "NAME2", null);
        PROJECT_LIST.add(project1);
        PROJECT_LIST.add(project2);

        Assert.assertEquals(expectedNumberOfEntries, PROJECT_SERVICE.getSize());
        Assert.assertEquals("NAME1", project1.getName());
        Assert.assertEquals("DESCRIPTION", project1.getDescription());
        Assert.assertEquals(user1.getId(), project1.getUserId());

        Assert.assertEquals("NAME2", project2.getName());
        Assert.assertEquals("", project2.getDescription());
        Assert.assertEquals(user2.getId(), project2.getUserId());
    }

    @Test
    public void testCrateProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.create(null, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(NameEmptyException.class, () -> PROJECT_SERVICE.create(UNKNOWN_ID, null, UNKNOWN_ID));
        Assert.assertThrows(NameEmptyException.class, () -> PROJECT_SERVICE.create(UNKNOWN_ID, null, null));
    }

    @Test
    public void testUpdateById() throws Exception {
        @NotNull final Project project = PROJECT_SERVICE.findAll(user1.getId()).get(0);
        Assert.assertNotNull(PROJECT_SERVICE.updateById(user1.getId(), project.getId(), "NEW N", "NEW D"));
        @Nullable final Project actualProject = PROJECT_SERVICE.findOneById(project.getId());
        Assert.assertNotNull(actualProject);
        Assert.assertEquals("NEW N", actualProject.getName());
        Assert.assertEquals("NEW D", actualProject.getDescription());
    }

    @Test
    public void testUpdateByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.updateById(null, UNKNOWN_ID, UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.updateById(UNKNOWN_ID, null, UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(ProjectNotFoundException.class, () -> PROJECT_SERVICE.updateById(UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_STRING, UNKNOWN_STRING));
    }

    @Test
    public void testUpdateByIndex() throws Exception {
        @NotNull final Project project = PROJECT_SERVICE.findAll(user1.getId()).get(0);
        Assert.assertNotNull(PROJECT_SERVICE.updateByIndex(user1.getId(), 1, "NEW N", "NEW D"));
        @Nullable final Project actualProject = PROJECT_SERVICE.findOneById(project.getId());
        Assert.assertNotNull(actualProject);
        Assert.assertEquals("NEW N", actualProject.getName());
        Assert.assertEquals("NEW D", actualProject.getDescription());
    }

    @Test
    public void testUpdateByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.updateByIndex(null, 0, UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.updateByIndex(UNKNOWN_ID, -1, UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.updateByIndex(UNKNOWN_ID, null, UNKNOWN_STRING, UNKNOWN_STRING));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.updateByIndex(UNKNOWN_ID, NUMBER_OF_PROJECTS, UNKNOWN_STRING, UNKNOWN_STRING));
    }

    @Test
    public void testChangeProjectStatusById() throws Exception {
        @NotNull final Project project = PROJECT_SERVICE.findAll(user1.getId()).get(0);
        Assert.assertNotNull(PROJECT_SERVICE.changeProjectStatusById(user1.getId(), project.getId(), Status.COMPLETED));
        @Nullable Project actualProject = PROJECT_SERVICE.findOneById(project.getId());
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(Status.COMPLETED, actualProject.getStatus());
        Assert.assertNotNull(PROJECT_SERVICE.changeProjectStatusById(user1.getId(), project.getId(), null));
        actualProject = PROJECT_SERVICE.findOneById(project.getId());
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(Status.COMPLETED, actualProject.getStatus());
    }

    @Test
    public void testChangeProjectStatusByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.changeProjectStatusById(null, UNKNOWN_ID, Status.COMPLETED));
        Assert.assertThrows(IdEmptyException.class, () -> PROJECT_SERVICE.changeProjectStatusById(UNKNOWN_ID, null, Status.COMPLETED));
        Assert.assertThrows(ProjectNotFoundException.class, () -> PROJECT_SERVICE.changeProjectStatusById(UNKNOWN_ID, UNKNOWN_ID, Status.COMPLETED));
    }

    @Test
    public void testChangeProjectStatusByIndex() throws Exception {
        @NotNull final Project project = PROJECT_SERVICE.findAll(user1.getId()).get(0);
        Assert.assertNotNull(PROJECT_SERVICE.changeProjectStatusByIndex(user1.getId(), 1, Status.COMPLETED));
        @Nullable Project actualProject = PROJECT_SERVICE.findOneById(project.getId());
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(Status.COMPLETED, actualProject.getStatus());
        Assert.assertNotNull(PROJECT_SERVICE.changeProjectStatusByIndex(user1.getId(), 1, null));
        actualProject = PROJECT_SERVICE.findOneById(project.getId());
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(Status.COMPLETED, actualProject.getStatus());
    }

    @Test
    public void testChangeProjectStatusByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.changeProjectStatusByIndex(null, 0, Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.changeProjectStatusByIndex(UNKNOWN_ID, -1, Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.changeProjectStatusByIndex(UNKNOWN_ID, NUMBER_OF_PROJECTS, Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.changeProjectStatusByIndex(UNKNOWN_ID, null, Status.COMPLETED));
    }

    @Test
    public void testRemoveProjectByIdForUser() throws Exception {
        @NotNull final String projectId = PROJECT_REPOSITORY.findAll(user1.getId()).get(0).getId();
        @NotNull final List<Task> taskListToRemove = TASK_REPOSITORY.findAllByProjectId(user1.getId(), projectId);
        final int expectedNumberOfProjects = NUMBER_OF_PROJECTS - 1;
        final int expectedNumberOfTasks = TASK_REPOSITORY.getSize(user1.getId()) - taskListToRemove.size();
        PROJECT_SERVICE.removeById(user1.getId(), projectId);
        Assert.assertEquals(expectedNumberOfProjects, PROJECT_REPOSITORY.getSize());
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(user1.getId(), projectId));
        Assert.assertEquals(expectedNumberOfTasks, TASK_REPOSITORY.findAll(user1.getId()).size());
    }

    @Test
    public void testRemoveProjectByIdForUserNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> PROJECT_SERVICE.removeById(null, UNKNOWN_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> PROJECT_SERVICE.removeById(UNKNOWN_ID, null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> PROJECT_SERVICE.removeById(UNKNOWN_ID, UNKNOWN_ID));
    }

    @Test
    public void testRemoveProjectByIndexForUser() throws Exception {
        final int index = 1;
        @Nullable final Project project = PROJECT_REPOSITORY.findOneByIndex(user1.getId(), 1);
        Assert.assertNotNull(project);
        @NotNull final String projectId = project.getId();
        @NotNull final List<Task> taskListToRemove = TASK_REPOSITORY.findAllByProjectId(user1.getId(), projectId);
        final int expectedNumberOfProjects = NUMBER_OF_PROJECTS - 1;
        final int expectedNumberOfTasks = TASK_REPOSITORY.getSize(user1.getId()) - taskListToRemove.size();
        PROJECT_SERVICE.removeByIndex(user1.getId(), index);
        Assert.assertEquals(expectedNumberOfProjects, PROJECT_REPOSITORY.getSize());
        Assert.assertNull(PROJECT_REPOSITORY.findOneById(user1.getId(), projectId));
        Assert.assertEquals(expectedNumberOfTasks, TASK_REPOSITORY.findAll(user1.getId()).size());
    }

    @Test
    public void testRemoveProjectByIndexForUserNegative() {
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.removeByIndex(UNKNOWN_ID, -1));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.removeByIndex(UNKNOWN_ID, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> PROJECT_SERVICE.removeByIndex(UNKNOWN_ID, NUMBER_OF_PROJECTS));
    }

}