package ru.t1.malyugin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.endpoint.IDomainEndpoint;
import ru.t1.malyugin.tm.api.service.IDomainService;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.dto.request.data.load.*;
import ru.t1.malyugin.tm.dto.request.data.save.*;
import ru.t1.malyugin.tm.dto.response.data.load.*;
import ru.t1.malyugin.tm.dto.response.data.save.*;
import ru.t1.malyugin.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.malyugin.tm.api.endpoint.IDomainEndpoint")
public final class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    public DomainEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IDomainService getDomainService() {
        return getServiceLocator().getDomainService();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupLoadResponse loadBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupLoadRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        getDomainService().loadDataBackup();
        return new DataBackupLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBackupSaveResponse saveBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBackupSaveRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        getDomainService().saveDataBackup();
        return new DataBackupSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64LoadResponse loadBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64LoadRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        getDomainService().loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64SaveResponse saveBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBase64SaveRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        getDomainService().saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinaryLoadResponse loadBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinaryLoadRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        getDomainService().loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinarySaveResponse saveBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataBinarySaveRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        getDomainService().saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonFasterXmlLoadResponse loadJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonFasterXmlLoadRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        getDomainService().loadDataJsonFasterXml();
        return new DataJsonFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonFasterXmlSaveResponse saveJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonFasterXmlSaveRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        getDomainService().saveDataJsonFasterXml();
        return new DataJsonFasterXmlSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonJaxBLoadResponse loadJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonJaxBLoadRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        getDomainService().loadDataJsonJaxB();
        return new DataJsonJaxBLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonJaxBSaveResponse saveJsonJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataJsonJaxBSaveRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        getDomainService().saveDataJsonJaxB();
        return new DataJsonJaxBSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlFasterXmlLoadResponse loadXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlFasterXmlLoadRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        getDomainService().loadDataXmlFasterXml();
        return new DataXmlFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlFasterXmlSaveResponse saveXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlFasterXmlSaveRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        getDomainService().saveDataXmlFasterXml();
        return new DataXmlFasterXmlSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlJaxBLoadResponse loadXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlJaxBLoadRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        getDomainService().loadDataXmlJaxB();
        return new DataXmlJaxBLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlJaxBSaveResponse saveXmlJaxB(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataXmlJaxBSaveRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        getDomainService().saveDataXmlJaxB();
        return new DataXmlJaxBSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlFasterXmlLoadResponse loadYaml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlFasterXmlLoadRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        getDomainService().loadDataYamlFasterXml();
        return new DataYamlFasterXmlLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlFasterXmlSaveResponse saveYaml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DataYamlFasterXmlSaveRequest request
    ) {
        checkPermission(request, Role.ADMIN);
        getDomainService().saveDataYamlFasterXml();
        return new DataYamlFasterXmlSaveResponse();
    }

}