package ru.t1.malyugin.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.service.*;
import ru.t1.malyugin.tm.service.*;

public final class ServiceLocator implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService, this);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService, this);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(connectionService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(connectionService, this);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(this);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService(propertyService);

}