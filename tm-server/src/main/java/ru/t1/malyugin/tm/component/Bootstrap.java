package ru.t1.malyugin.tm.component;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.endpoint.*;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.endpoint.*;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@NoArgsConstructor
public final class Bootstrap {

    @NotNull
    private final IServiceLocator serviceLocator = new ServiceLocator();

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(serviceLocator);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(serviceLocator);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(serviceLocator);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(serviceLocator);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(serviceLocator);

    {
        registryEndpoint(systemEndpoint);
        registryEndpoint(domainEndpoint);
        registryEndpoint(projectEndpoint);
        registryEndpoint(taskEndpoint);
        registryEndpoint(userEndpoint);
        registryEndpoint(authEndpoint);
    }

    private void registryEndpoint(@NotNull final Object endpoint) {
        @NotNull final String host = serviceLocator.getPropertyService().getServerHost();
        @NotNull final String port = serviceLocator.getPropertyService().getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        try {
            serviceLocator.getSessionService().clear();
            serviceLocator.getTaskService().clear();
            serviceLocator.getProjectService().clear();
            serviceLocator.getUserService().clear();
            serviceLocator.getUserService().create("admin", "admin", "admin@m.ru", Role.ADMIN);
            serviceLocator.getUserService().create("soap_tst_user", "soap_pass", "soap@mail.ru", Role.USUAL);
        } catch (@NotNull final Exception e) {
            e.printStackTrace();
        }

    }

    private void initLogger() {
        serviceLocator.getLoggerService().info("** WELCOME TO TM SERVER **");
    }

    private void initLog4j() {
        BasicConfigurator.configure();
    }

    private void prepareStartup() {
        initPID();
        initDemoData();
        initLogger();
        initLog4j();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        serviceLocator.getLoggerService().info("** TM SERVER IS SHUTTING DOWN **");
    }

    public void run(@Nullable final String... args) {
        prepareStartup();
    }

}