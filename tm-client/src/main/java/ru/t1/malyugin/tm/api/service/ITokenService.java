package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ITokenService {

    @NotNull
    String getToken();

    void setToken(@NotNull String token);

}