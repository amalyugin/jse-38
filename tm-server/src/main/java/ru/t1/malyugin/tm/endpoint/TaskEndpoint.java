package ru.t1.malyugin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.endpoint.ITaskEndpoint;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.dto.request.task.*;
import ru.t1.malyugin.tm.dto.response.task.*;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.server.EndpointException;
import ru.t1.malyugin.tm.model.Session;
import ru.t1.malyugin.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.t1.malyugin.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }


    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIdRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final Task task;
        try {
            task = getTaskService().changeTaskStatusById(userId, id, status);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskChangeStatusByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskChangeStatusByIndexRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer taskIndex = request.getTaskIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final Task task;
        try {
            task = getTaskService().changeTaskStatusByIndex(userId, taskIndex, status);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskChangeStatusByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIdResponse completeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIdRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Task task;
        try {
            task = getTaskService().changeTaskStatusById(userId, id, Status.COMPLETED);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskCompleteByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCompleteByIndexResponse completeTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCompleteByIndexRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer taskIndex = request.getIndex();
        @Nullable final Task task;
        try {
            task = getTaskService().changeTaskStatusByIndex(userId, taskIndex, Status.COMPLETED);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskCompleteByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIdResponse startTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIdRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Task task;
        try {
            task = getTaskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskStartByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskStartByIndexResponse startTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskStartByIndexRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer taskIndex = request.getIndex();
        @Nullable final Task task;
        try {
            task = getTaskService().changeTaskStatusByIndex(userId, taskIndex, Status.IN_PROGRESS);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskStartByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIdRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task;
        try {
            task = getTaskService().updateById(userId, id, name, description);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskUpdateByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIndexResponse updateTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUpdateByIndexRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task;
        try {
            task = getTaskService().updateByIndex(userId, index, name, description);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskUpdateByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIdRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        try {
            getTaskService().removeById(userId, id);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskRemoveByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIndexResponse removeTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskRemoveByIndexRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer taskIndex = request.getIndex();
        try {
            getTaskService().removeByIndex(userId, taskIndex);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskRemoveByIndexResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCreateResponse creteTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskCreateRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final Task task;
        try {
            task = getTaskService().create(userId, name, description);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskCreateResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskClearRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        try {
            getTaskService().clear(userId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByIdResponse showTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIdRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Task task;
        try {
            task = getTaskService().findOneById(userId, id);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskShowByIdResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowByIndexResponse showTaskByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowByIndexRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Task task;
        try {
            task = getTaskService().findOneByIndex(userId, index);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskShowByIndexResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowListResponse showTaskList(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowListRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSort();
        @Nullable final List<Task> taskList;
        try {
            taskList = getTaskService().findAll(userId, sort);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskShowListResponse(taskList);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskShowListByProjectIdResponse showTaskListByProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskShowListByProjectIdRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final List<Task> taskList;
        try {
            taskList = getTaskService().findAllByProjectId(userId, projectId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskShowListByProjectIdResponse(taskList);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskBindToProjectRequest request
    ) {
        checkPermission(request);
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final Task task;
        try {
            task = getTaskService().bindTaskToProject(userId, projectId, taskId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskBindToProjectResponse(task);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUnbindToProjectResponse unbindTaskFromProject(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final TaskUnbindFromProjectRequest request
    ) {
        @NotNull final Session session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final Task task;
        try {
            task = getTaskService().unbindTaskFromProject(userId, projectId, taskId);
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new TaskUnbindToProjectResponse(task);
    }

}