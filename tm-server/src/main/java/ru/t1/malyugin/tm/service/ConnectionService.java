package ru.t1.malyugin.tm.service;

import lombok.SneakyThrows;
import org.apache.commons.dbcp2.BasicDataSource;
import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.service.IConnectionService;
import ru.t1.malyugin.tm.api.service.IPropertyService;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final BasicDataSource dataSource;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.dataSource = dataSource();
    }

    public BasicDataSource dataSource() {
        final BasicDataSource result = new BasicDataSource();
        result.setUrl(propertyService.getDatabaseUrl());
        result.setUsername(propertyService.getDatabaseUsername());
        result.setPassword(propertyService.getDatabasePassword());
        result.setMinIdle(5);
        result.setMaxIdle(10);
        result.setAutoCommitOnReturn(false);
        result.setMaxOpenPreparedStatements(100);
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection(final boolean autoCommit) {
        @NotNull final Connection connection = dataSource.getConnection();
        connection.setAutoCommit(autoCommit);
        return connection;
    }

}