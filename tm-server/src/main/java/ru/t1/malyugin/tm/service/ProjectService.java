package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.api.service.IConnectionService;
import ru.t1.malyugin.tm.api.service.IProjectService;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.field.*;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.util.List;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public ProjectService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IServiceLocator serviceLocator
    ) {
        super(connectionService);
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    protected IProjectRepository getRepository(@NotNull Connection connection) {
        return new ProjectRepository(connection);
    }

    private void updateProject(@NotNull final Project project) throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository repository = getRepository(connection);
        try {
            repository.update(project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        @NotNull final Project project = new Project();
        project.setUserId(userId.trim());
        project.setName(name.trim());
        if (!StringUtils.isBlank(description)) project.setDescription(description.trim());
        @NotNull final Connection connection = getConnection();
        @NotNull final IProjectRepository repository = getRepository(connection);
        try {
            repository.add(userId.trim(), project);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return project;
    }

    @NotNull
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final Project project = findOneById(userId.trim(), id.trim());
        if (project == null) throw new ProjectNotFoundException();
        if (!StringUtils.isBlank(name)) project.setName(name.trim());
        if (!StringUtils.isBlank(description)) project.setDescription(description.trim());
        updateProject(project);
        return project;
    }

    @NotNull
    @Override
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Project project = findOneByIndex(userId.trim(), index);
        if (project == null) throw new ProjectNotFoundException();
        if (!StringUtils.isBlank(name)) project.setName(name.trim());
        if (!StringUtils.isBlank(description)) project.setDescription(description.trim());
        updateProject(project);
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final Project project = findOneById(userId.trim(), id.trim());
        if (project == null) throw new ProjectNotFoundException();
        if (status != null) project.setStatus(status);
        updateProject(project);
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        if (status != null) project.setStatus(status);
        updateProject(project);
        return project;
    }

    @NotNull
    @Override
    public Project removeById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) throw new ProjectIdEmptyException();
        @Nullable final Project project = findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final List<Task> taskList = serviceLocator.getTaskService().findAllByProjectId(userId, projectId);
        serviceLocator.getTaskService().removeAll(userId, taskList);
        return super.remove(userId, project);
    }

    @NotNull
    @Override
    public Project removeByIndex(
            @Nullable final String userId,
            @Nullable final Integer index
    ) throws Exception {
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        return removeById(userId, project.getId());
    }

}