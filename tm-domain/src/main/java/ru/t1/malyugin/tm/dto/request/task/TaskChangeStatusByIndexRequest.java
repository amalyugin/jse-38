package ru.t1.malyugin.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.request.AbstractUserRequest;
import ru.t1.malyugin.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public final class TaskChangeStatusByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer taskIndex;

    @Nullable
    private Status status;

    public TaskChangeStatusByIndexRequest(
            @Nullable final String token,
            @Nullable final Integer taskIndex,
            @Nullable final Status status
    ) {
        super(token);
        this.taskIndex = taskIndex;
        this.status = status;
    }

}