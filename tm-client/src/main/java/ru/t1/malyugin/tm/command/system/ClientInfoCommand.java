package ru.t1.malyugin.tm.command.system;

import org.jetbrains.annotations.NotNull;

import static ru.t1.malyugin.tm.util.FormatUtil.formatBytes;

public final class ClientInfoCommand extends AbstractSystemCommand {

    @NotNull
    private static final String NAME = "info-client";

    @NotNull
    private static final String DESCRIPTION = "Show client info";

    @NotNull
    private static final String ARGUMENT = "-i";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLIENT INFO]");

        final int processorsCount = Runtime.getRuntime().availableProcessors();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;

        System.out.println("PROCESSORS: " + processorsCount);
        System.out.println("MAX MEMORY: " + formatBytes(maxMemory));
        System.out.println("TOTAL MEMORY: " + formatBytes(totalMemory));
        System.out.println("FREE MEMORY: " + formatBytes(freeMemory));
        System.out.println("USED MEMORY: " + formatBytes(usedMemory));
    }

}