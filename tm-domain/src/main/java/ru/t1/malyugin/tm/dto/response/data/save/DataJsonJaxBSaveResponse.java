package ru.t1.malyugin.tm.dto.response.data.save;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.malyugin.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public final class DataJsonJaxBSaveResponse extends AbstractResponse {

}