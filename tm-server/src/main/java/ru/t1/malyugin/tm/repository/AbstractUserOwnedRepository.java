package ru.t1.malyugin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.IUserOwnedRepository;
import ru.t1.malyugin.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M>
        implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public abstract M add(@NotNull final String userId, @NotNull final M model) throws Exception;

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ?", getTableName(), DBConstant.COLUMN_USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @NotNull final Comparator comparator) throws Exception {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? ORDER BY %s",
                getTableName(),
                DBConstant.COLUMN_USER_ID,
                getSortType(comparator)
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @NotNull final String id) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? AND %s = ?",
                getTableName(),
                DBConstant.COLUMN_USER_ID,
                DBConstant.COLUMN_ROW_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            else return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @NotNull final Integer index) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT ?", getTableName(), DBConstant.COLUMN_USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setInt(2, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            for (int i = 0; i < index - 1; i++) resultSet.next();
            return fetch(resultSet);
        }
    }

    @Override
    public void clear(@NotNull final String userId) throws Exception {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE %s = ?", getTableName(), DBConstant.COLUMN_USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final M model) throws Exception {
        @NotNull final String sql = String.format("DELETE FROM %s WHERE %s = ? AND %s = ?",
                getTableName(), DBConstant.COLUMN_USER_ID, DBConstant.COLUMN_ROW_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, model.getId());
            statement.executeUpdate();
        }
    }

    @Override
    public int getSize(@NotNull final String userId) throws Exception {
        @NotNull final String sql = String.format("SELECT COUNT(*) FROM %s WHERE %s = ?", getTableName(), DBConstant.COLUMN_USER_ID);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt("count");
        }
    }

    @Override
    public void removeAll(@NotNull final String userId, @NotNull final Collection<M> models) throws Exception {
        for (@NotNull final M model : models) remove(userId, model);
    }

}