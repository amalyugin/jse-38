package ru.t1.malyugin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.request.project.ProjectShowListRequest;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectListShowCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-list";

    @NotNull
    private static final String DESCRIPTION = "Show project list";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT LIST]");

        System.out.print("ENTER SORT: ");
        System.out.print(Sort.renderValuesList());
        @Nullable final Integer sortIndex = TerminalUtil.nextIntegerSafe();
        @Nullable final Sort sort = Sort.getSortByIndex(sortIndex);

        @NotNull final ProjectShowListRequest request = new ProjectShowListRequest(getToken(), sort);
        @Nullable final List<Project> projects = getProjectEndpoint().showProjectList(request).getProjectList();
        renderProjectList(projects);
    }

}