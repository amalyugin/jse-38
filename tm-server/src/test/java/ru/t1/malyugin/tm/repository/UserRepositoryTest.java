package ru.t1.malyugin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.util.HashUtil;

import static ru.t1.malyugin.tm.TestData.*;

@Category(UnitCategory.class)
public class UserRepositoryTest {

    @Before
    public void initRepository() throws Exception {
        for (int i = 1; i <= NUMBER_OF_USERS; i++) {
            @NotNull final User user = new User();
            user.setLogin("LOGIN " + i);
            @Nullable final String hash = HashUtil.salt(PROPERTY_SERVICE, "password" + i);
            Assert.assertNotNull(hash);
            user.setPasswordHash(hash);
            user.setEmail("mail@" + i);
            USER_REPOSITORY.add(user);
            USER_LIST.add(user);
        }
    }

    @After
    public void clearData() throws Exception {
        TASK_SERVICE.clear();
        PROJECT_SERVICE.clear();
        USER_SERVICE.removeAll(USER_LIST);
        TASK_LIST.clear();
        PROJECT_LIST.clear();
        USER_LIST.clear();
    }

    @Test
    public void testFindOneByLogin() throws Exception {
        for (@NotNull final User user : USER_LIST) {
            Assert.assertEquals(user.getId(), USER_REPOSITORY.findOneByLogin(user.getLogin()).getId());
        }
        @NotNull final String login = "TEST_LOGIN";
        Assert.assertNull(USER_REPOSITORY.findOneByLogin(login));
    }

    @Test
    public void testFindOneByEmail() throws Exception {
        for (@NotNull final User user : USER_LIST) {
            if (user.getEmail() != null)
                Assert.assertEquals(user.getId(), USER_REPOSITORY.findOneByEmail(user.getEmail()).getId());
        }
        @NotNull final String email = "TEST_EMAIL";
        Assert.assertNull(USER_REPOSITORY.findOneByEmail(email));
    }

    @Test
    public void testUpdate() throws Exception {
        for (@NotNull final User user : USER_LIST) {
            final boolean newLocked = !user.getLocked();
            @NotNull final String newFirstName = "Иван";
            @NotNull final String newMiddleName = "Иванович";
            @NotNull final String newLastName = "Иванов";
            @NotNull final String newPassHash = "PASS_HASH";
            @NotNull final Role newRole = Role.ADMIN;
            user.setLocked(newLocked);
            user.setFirstName(newFirstName);
            user.setMiddleName(newMiddleName);
            user.setLastName(newLastName);
            user.setPasswordHash(newPassHash);
            user.setRole(newRole);
            USER_REPOSITORY.update(user);
            @Nullable final User newUser = USER_REPOSITORY.findOneById(user.getId());
            Assert.assertNotNull(newUser);
            Assert.assertEquals(newLocked, newUser.getLocked());
            Assert.assertEquals(newFirstName, newUser.getFirstName());
            Assert.assertEquals(newMiddleName, newUser.getMiddleName());
            Assert.assertEquals(newLastName, newUser.getLastName());
            Assert.assertEquals(newPassHash, newUser.getPasswordHash());
            Assert.assertEquals(newRole, newUser.getRole());
        }
    }

}