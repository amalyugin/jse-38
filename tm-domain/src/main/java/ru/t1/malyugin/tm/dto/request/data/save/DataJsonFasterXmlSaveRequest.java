package ru.t1.malyugin.tm.dto.request.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.request.AbstractUserRequest;

@NoArgsConstructor
public final class DataJsonFasterXmlSaveRequest extends AbstractUserRequest {

    public DataJsonFasterXmlSaveRequest(@Nullable final String token) {
        super(token);
    }

}