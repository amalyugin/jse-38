package ru.t1.malyugin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.repository.ISessionRepository;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.model.Session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return DBConstant.TABLE_SESSION;
    }

    @NotNull
    @Override
    public Session fetch(@NotNull final ResultSet row) throws Exception {
        @NotNull final Session session = new Session();
        session.setId(row.getString(DBConstant.COLUMN_ROW_ID));
        session.setDate(row.getTimestamp(DBConstant.COLUMN_CREATED));
        session.setUserId(row.getString(DBConstant.COLUMN_USER_ID));
        if (row.getString(DBConstant.COLUMN_ROLE) != null)
            session.setRole(Role.valueOf(row.getString(DBConstant.COLUMN_ROLE)));
        else session.setRole(null);
        return session;
    }

    @NotNull
    @Override
    public Session add(@NotNull final Session session) throws Exception {
        @NotNull final String sql = String.format("INSERT INTO %s (%s, %s, %s, %s) VALUES (?, ?, ?, ?)",
                getTableName(), DBConstant.COLUMN_ROW_ID, DBConstant.COLUMN_USER_ID,
                DBConstant.COLUMN_CREATED, DBConstant.COLUMN_ROLE
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getId());
            statement.setString(2, session.getUserId());
            statement.setTimestamp(3, new Timestamp(session.getDate().getTime()));
            if (session.getRole() != null) statement.setString(4, session.getRole().toString());
            else statement.setString(4, null);
            statement.executeUpdate();
        }
        return session;
    }

    @NotNull
    @Override
    public Session add(@NotNull final String userId, @NotNull final Session session) throws Exception {
        session.setUserId(userId);
        return add(session);
    }

}