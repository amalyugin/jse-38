package ru.t1.malyugin.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.dto.response.AbstractResponse;
import ru.t1.malyugin.tm.model.Task;


@Getter
@Setter
@NoArgsConstructor
public class TaskShowByIdResponse extends AbstractResponse {

    @Nullable
    private Task task;

    public TaskShowByIdResponse(@Nullable final Task task) {
        this.task = task;
    }

}