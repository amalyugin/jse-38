package ru.t1.malyugin.tm;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.repository.IProjectRepository;
import ru.t1.malyugin.tm.api.repository.ISessionRepository;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.api.repository.IUserRepository;
import ru.t1.malyugin.tm.api.service.*;
import ru.t1.malyugin.tm.component.ServiceLocator;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Session;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.repository.ProjectRepository;
import ru.t1.malyugin.tm.repository.SessionRepository;
import ru.t1.malyugin.tm.repository.TaskRepository;
import ru.t1.malyugin.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public interface TestData {

    int NUMBER_OF_PROJECTS = 10;

    int NUMBER_OF_TASKS = 10;

    int NUMBER_OF_USERS = 10;

    int NUMBER_OF_SESSION = 10;

    @NotNull
    String FIRST_USUAL_USER_LOGIN = "TST_USER_1";

    @NotNull
    String FIRST_USUAL_USER_PASS = "TST_PASS_1";

    @NotNull
    String SECOND_USUAL_USER_LOGIN = "TST_USER_2";

    @NotNull
    String SECOND_USUAL_USER_PASS = "TST_PASS_2";

    @NotNull
    String UNKNOWN_ID = UUID.randomUUID().toString();

    @NotNull
    String UNKNOWN_STRING = "SOME_STR";

    @NotNull
    List<Project> PROJECT_LIST = new ArrayList<>();

    @NotNull
    List<Task> TASK_LIST = new ArrayList<>();

    @NotNull
    List<User> USER_LIST = new ArrayList<>();

    @NotNull
    List<Session> SESSION_LIST = new ArrayList<>();

    @NotNull
    IServiceLocator SERVICE_LOCATOR = new ServiceLocator();

    @NotNull
    IPropertyService PROPERTY_SERVICE = SERVICE_LOCATOR.getPropertyService();

    @NotNull
    ISessionService SESSION_SERVICE = SERVICE_LOCATOR.getSessionService();

    @NotNull
    ITaskService TASK_SERVICE = SERVICE_LOCATOR.getTaskService();

    @NotNull
    IProjectService PROJECT_SERVICE = SERVICE_LOCATOR.getProjectService();

    @NotNull
    IUserService USER_SERVICE = SERVICE_LOCATOR.getUserService();

    @NotNull
    IConnectionService CONNECTION_SERVICE = SERVICE_LOCATOR.getConnectionService();

    @NotNull
    IProjectRepository PROJECT_REPOSITORY = new ProjectRepository(CONNECTION_SERVICE.getConnection(true));

    @NotNull
    ITaskRepository TASK_REPOSITORY = new TaskRepository(CONNECTION_SERVICE.getConnection(true));

    @NotNull
    IUserRepository USER_REPOSITORY = new UserRepository(CONNECTION_SERVICE.getConnection(true));

    @NotNull
    ISessionRepository SESSION_REPOSITORY = new SessionRepository(CONNECTION_SERVICE.getConnection(true));

}