package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.api.service.IConnectionService;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.api.service.ITaskService;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.entity.TaskNotFoundException;
import ru.t1.malyugin.tm.exception.field.*;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.Collections;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public TaskService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IServiceLocator serviceLocator
    ) {
        super(connectionService);
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    protected ITaskRepository getRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    private void updateTask(@NotNull final Task task) throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = getRepository(connection);
        try {
            repository.update(task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        @NotNull final Task task = new Task();
        task.setUserId(userId.trim());
        task.setName(name.trim());
        if (!StringUtils.isBlank(description)) task.setDescription(description.trim());
        @NotNull final Connection connection = getConnection();
        @NotNull final ITaskRepository repository = getRepository(connection);
        try {
            repository.add(userId, task);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return task;
    }

    @Override
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        @Nullable final Task task = findOneById(userId.trim(), id.trim());
        if (task == null) throw new TaskNotFoundException();
        task.setName(name.trim());
        if (!StringUtils.isBlank(description)) task.setDescription(description.trim());
        updateTask(task);
        return task;
    }

    @Override
    public Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        if (StringUtils.isBlank(name)) throw new NameEmptyException();
        @Nullable final Task task = findOneByIndex(userId.trim(), index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name.trim());
        if (!StringUtils.isBlank(description)) task.setDescription(description.trim());
        updateTask(task);
        return task;
    }

    @Override
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final Task task = findOneById(userId.trim(), id.trim());
        if (task == null) throw new TaskNotFoundException();
        if (status != null) task.setStatus(status);
        updateTask(task);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Task task = findOneByIndex(userId.trim(), index);
        if (task == null) throw new TaskNotFoundException();
        if (status != null) task.setStatus(status);
        updateTask(task);
        return task;
    }

    @NotNull
    @Override
    public Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) throw new ProjectIdEmptyException();
        if (StringUtils.isBlank(taskId)) throw new TaskIdEmptyException();
        if (serviceLocator.getProjectService().findOneById(userId.trim(), projectId.trim()) == null)
            throw new ProjectNotFoundException();
        @Nullable final Task task = findOneById(userId.trim(), taskId.trim());
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId.trim());
        updateTask(task);
        return task;
    }

    @NotNull
    @Override
    public Task unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) throw new ProjectIdEmptyException();
        if (StringUtils.isBlank(taskId)) throw new TaskIdEmptyException();
        if (serviceLocator.getProjectService().findOneById(userId.trim(), projectId.trim()) == null)
            throw new ProjectNotFoundException();
        @Nullable final Task task = findOneById(userId.trim(), taskId.trim());
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        updateTask(task);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(projectId)) return Collections.emptyList();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final ITaskRepository repository = getRepository(connection);
            return repository.findAllByProjectId(userId.trim(), projectId.trim());
        }
    }

}