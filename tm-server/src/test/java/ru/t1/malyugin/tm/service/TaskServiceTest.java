package ru.t1.malyugin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.malyugin.tm.exception.entity.TaskNotFoundException;
import ru.t1.malyugin.tm.exception.field.*;
import ru.t1.malyugin.tm.marker.UnitCategory;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.model.Task;
import ru.t1.malyugin.tm.model.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static ru.t1.malyugin.tm.TestData.*;

@Category(UnitCategory.class)
public class TaskServiceTest {

    @NotNull
    private static User user1 = new User();

    @NotNull
    private static User user2 = new User();

    @BeforeClass
    public static void initUser() throws Exception {
        user1 = USER_SERVICE.create(FIRST_USUAL_USER_LOGIN, FIRST_USUAL_USER_PASS, null, Role.USUAL);
        user2 = USER_SERVICE.create(SECOND_USUAL_USER_LOGIN, SECOND_USUAL_USER_PASS, null, Role.USUAL);
    }

    @AfterClass
    public static void clearUser() throws Exception {
        USER_SERVICE.remove(user1);
        USER_SERVICE.remove(user2);
    }

    @NotNull
    private Project project = new Project();

    @Before
    public void initTest() throws Exception {
        project = new Project("TST_P", "TST_D");
        project.setUserId(user1.getId());
        PROJECT_REPOSITORY.add(project);

        for (int i = 1; i <= NUMBER_OF_TASKS; i++) {
            @NotNull final Task task = new Task("P" + i, "D" + i);
            if (i <= NUMBER_OF_TASKS / 2) task.setUserId(user1.getId());
            else task.setUserId(user2.getId());
            if (i <= 3) task.setProjectId(project.getId());
            TASK_REPOSITORY.add(task);
            TASK_LIST.add(task);
        }
    }

    @After
    public void clearData() throws Exception {
        TASK_SERVICE.clear();
        PROJECT_SERVICE.clear();
        TASK_LIST.clear();
        PROJECT_LIST.clear();
    }

    @Test
    public void testCrateTask() throws Exception {
        final int expectedNumberOfEntries = NUMBER_OF_TASKS + 2;
        @NotNull final Task task1 = TASK_SERVICE.create(user1.getId(), "NAME1", "DESCRIPTION");
        @NotNull final Task task2 = TASK_SERVICE.create(user2.getId(), "NAME2", null);

        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
        Assert.assertEquals("NAME1", task1.getName());
        Assert.assertEquals("DESCRIPTION", task1.getDescription());
        Assert.assertEquals(user1.getId(), task1.getUserId());

        Assert.assertEquals("NAME2", task2.getName());
        Assert.assertEquals("", task2.getDescription());
        Assert.assertEquals(user2.getId(), task2.getUserId());
    }

    @Test
    public void testCrateTaskNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create(null, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create(null, UNKNOWN_ID, null));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(UNKNOWN_ID, null, UNKNOWN_ID));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(UNKNOWN_ID, null, null));
    }

    @Test
    public void testUpdateById() throws Exception {
        @NotNull final Task task = TASK_SERVICE.findAll(user1.getId()).get(0);
        @NotNull final String test = "TEST";
        Assert.assertNotNull(TASK_SERVICE.updateById(user1.getId(), task.getId(), test, null));
        Assert.assertNotNull(TASK_SERVICE.updateById(user1.getId(), task.getId(), test, test));

        @Nullable final Task actualTask = TASK_SERVICE.findOneById(task.getId());
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(test, actualTask.getName());
        Assert.assertEquals(test, actualTask.getDescription());
    }

    @Test
    public void testUpdateByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.updateById(null, UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.updateById(UNKNOWN_ID, null, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(TaskNotFoundException.class, () -> TASK_SERVICE.updateById(UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.updateById(UNKNOWN_ID, UNKNOWN_ID, null, UNKNOWN_ID));
    }

    @Test
    public void testUpdateByIndex() throws Exception {
        @NotNull final int index = 1;
        @NotNull Task task = TASK_SERVICE.findOneByIndex(index);
        Assert.assertNotNull(task);
        @NotNull final String test = "TEST";
        Assert.assertNotNull(TASK_SERVICE.updateByIndex(user1.getId(), index, test, null));
        @Nullable Task actualTask = TASK_SERVICE.findOneById(task.getId());
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(test, actualTask.getName());

        task = TASK_SERVICE.findOneByIndex(index);
        Assert.assertNotNull(task);
        Assert.assertNotNull(TASK_SERVICE.updateByIndex(user1.getId(), index, test, test));
        actualTask = TASK_SERVICE.findOneById(task.getId());
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(test, actualTask.getName());
        Assert.assertEquals(test, actualTask.getDescription());
    }

    @Test
    public void testUpdateByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.updateByIndex(null, 0, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.updateByIndex(UNKNOWN_ID, -1, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.updateByIndex(UNKNOWN_ID, null, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.updateByIndex(UNKNOWN_ID, NUMBER_OF_TASKS, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.updateByIndex(user1.getId(), 1, null, UNKNOWN_ID));
    }

    @Test
    public void testChangeTaskStatusById() throws Exception {
        @NotNull final Task task = TASK_SERVICE.findAll(user1.getId()).get(0);
        Assert.assertNotNull(TASK_SERVICE.changeTaskStatusById(user1.getId(), task.getId(), Status.COMPLETED));
        @Nullable Task actualTask = TASK_SERVICE.findOneById(task.getId());
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(Status.COMPLETED, actualTask.getStatus());
        Assert.assertNotNull(TASK_SERVICE.changeTaskStatusById(user1.getId(), task.getId(), null));
        actualTask = TASK_SERVICE.findOneById(task.getId());
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(Status.COMPLETED, actualTask.getStatus());
    }

    @Test
    public void testChangeTaskStatusByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(null, UNKNOWN_ID, Status.COMPLETED));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(UNKNOWN_ID, null, Status.COMPLETED));
        Assert.assertThrows(TaskNotFoundException.class, () -> TASK_SERVICE.changeTaskStatusById(UNKNOWN_ID, UNKNOWN_ID, Status.COMPLETED));
    }

    @Test
    public void testChangeTaskStatusByIndex() throws Exception {
        @NotNull final Task task = TASK_SERVICE.findAll(user1.getId()).get(0);
        Assert.assertNotNull(TASK_SERVICE.changeTaskStatusByIndex(user1.getId(), 1, Status.COMPLETED));
        @Nullable Task actualTask = TASK_SERVICE.findOneById(task.getId());
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(Status.COMPLETED, actualTask.getStatus());
        Assert.assertNotNull(TASK_SERVICE.changeTaskStatusByIndex(user1.getId(), 1, null));
        actualTask = TASK_SERVICE.findOneById(task.getId());
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(Status.COMPLETED, actualTask.getStatus());
    }

    @Test
    public void testChangeTaskStatusByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusByIndex(null, 0, Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.changeTaskStatusByIndex(UNKNOWN_ID, -1, Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.changeTaskStatusByIndex(UNKNOWN_ID, NUMBER_OF_TASKS, Status.COMPLETED));
        Assert.assertThrows(IndexIncorrectException.class, () -> TASK_SERVICE.changeTaskStatusByIndex(UNKNOWN_ID, null, Status.COMPLETED));
    }

    @Test
    public void testBindTaskToProject() throws Exception {
        final String taskId = TASK_REPOSITORY.findAll(user1.getId()).get(4).getId();
        final int actualNumberOfEntries = TASK_REPOSITORY.findAllByProjectId(user1.getId(), project.getId()).size();
        final int expectedNumberOfEntries = actualNumberOfEntries + 1;
        Assert.assertNotNull(TASK_SERVICE.bindTaskToProject(user1.getId(), project.getId(), taskId));
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.findAllByProjectId(user1.getId(), project.getId()).size());
        @Nullable final Task task = TASK_REPOSITORY.findOneById(taskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(project.getId(), task.getProjectId());
    }

    @Test
    public void testBindTaskToProjectNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.bindTaskToProject(null, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> TASK_SERVICE.bindTaskToProject(UNKNOWN_ID, null, UNKNOWN_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> TASK_SERVICE.bindTaskToProject(UNKNOWN_ID, UNKNOWN_ID, null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> TASK_SERVICE.bindTaskToProject(UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_ID));
        @NotNull final String projectId = PROJECT_REPOSITORY.findAll(user1.getId()).get(0).getId();
        Assert.assertThrows(TaskNotFoundException.class, () -> TASK_SERVICE.bindTaskToProject(user1.getId(), projectId, UNKNOWN_ID));
    }

    @Test
    public void testUnbindTaskToProject() throws Exception {
        final String taskId = TASK_REPOSITORY.findAllByProjectId(user1.getId(), project.getId()).get(0).getId();
        final int actualNumberOfEntries = TASK_REPOSITORY.findAllByProjectId(user1.getId(), project.getId()).size();
        final int expectedNumberOfEntries = actualNumberOfEntries - 1;
        Assert.assertNotNull(TASK_SERVICE.unbindTaskFromProject(user1.getId(), project.getId(), taskId));
        Assert.assertEquals(expectedNumberOfEntries, TASK_REPOSITORY.findAllByProjectId(user1.getId(), project.getId()).size());
        Assert.assertNull(TASK_REPOSITORY.findOneById(taskId).getProjectId());
    }

    @Test
    public void testUnbindTaskToProjectNegative() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.unbindTaskFromProject(null, UNKNOWN_ID, UNKNOWN_ID));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> TASK_SERVICE.unbindTaskFromProject(UNKNOWN_ID, null, UNKNOWN_ID));
        Assert.assertThrows(TaskIdEmptyException.class, () -> TASK_SERVICE.unbindTaskFromProject(UNKNOWN_ID, UNKNOWN_ID, null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> TASK_SERVICE.unbindTaskFromProject(UNKNOWN_ID, UNKNOWN_ID, UNKNOWN_ID));
        @NotNull final String projectId = PROJECT_REPOSITORY.findAll(user1.getId()).get(0).getId();
        Assert.assertThrows(TaskNotFoundException.class, () -> TASK_SERVICE.unbindTaskFromProject(user1.getId(), projectId, UNKNOWN_ID));
    }

    @Test
    public void testFindAllByProjectId() throws Exception {
        @NotNull final List<Task> actualTaskList = TASK_SERVICE.findAllByProjectId(user1.getId(), project.getId());
        @NotNull final List<Task> expectedTaskList = TASK_LIST
                .stream()
                .filter(p -> project.getId().equals(p.getProjectId()))
                .collect(Collectors.toList());
        Assert.assertEquals(expectedTaskList.size(), actualTaskList.size());
        for (int i = 0; i < expectedTaskList.size(); i++) {
            Assert.assertEquals(expectedTaskList.get(i).getId(), actualTaskList.get(i).getId());
        }
        Assert.assertEquals(Collections.emptyList(), TASK_SERVICE.findAllByProjectId(user1.getId(), null));
    }

    @Test
    public void testFindAllByProjectIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.findAllByProjectId(null, UNKNOWN_ID));
    }

    @Test
    public void testRemoveAllForUser() throws Exception {
        final int expectedNumberOfEntries = NUMBER_OF_TASKS - 3;
        @NotNull final List<Task> actualUserTaskList = TASK_SERVICE.findAll(user1.getId());
        @NotNull final List<Task> actualUserTaskListOtherUser = TASK_SERVICE.findAll(user2.getId());
        @NotNull final List<Task> taskListToRemove = new ArrayList<>();
        taskListToRemove.add(actualUserTaskList.get(0));
        taskListToRemove.add(actualUserTaskList.get(1));
        taskListToRemove.add(actualUserTaskList.get(2));
        taskListToRemove.add(actualUserTaskListOtherUser.get(0));
        TASK_SERVICE.removeAll(user1.getId(), taskListToRemove);
        Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize());
    }

    @Test
    public void testRemoveByIdForUser() throws Exception {
        @NotNull final List<Task> actualUserTaskList = TASK_SERVICE.findAll(user1.getId());
        int expectedNumberOfEntries = actualUserTaskList.size();
        for (int i = 0; i < expectedNumberOfEntries; i++) {
            expectedNumberOfEntries--;
            @NotNull final Task task = actualUserTaskList.get(i);
            TASK_SERVICE.removeById(user1.getId(), task.getId());
            Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize(user1.getId()));
        }
    }

    @Test
    public void testRemoveByIndexForUser() throws Exception {
        @NotNull final List<Task> actualUserTaskList = TASK_SERVICE.findAll(user1.getId());
        int expectedNumberOfEntries = actualUserTaskList.size();
        for (int i = 0; i < expectedNumberOfEntries; i++) {
            expectedNumberOfEntries--;
            TASK_SERVICE.removeByIndex(user1.getId(), i + 1);
            Assert.assertEquals(expectedNumberOfEntries, TASK_SERVICE.getSize(user1.getId()));
        }
    }

}