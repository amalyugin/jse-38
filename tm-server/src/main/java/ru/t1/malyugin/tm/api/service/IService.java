package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IService<M extends AbstractModel> {

    @Nullable
    M add(@Nullable M model) throws Exception;

    @NotNull
    Collection<M> addAll(@Nullable Collection<M> models) throws Exception;

    @NotNull
    Collection<M> set(@Nullable Collection<M> models) throws Exception;

    void clear() throws Exception;

    @NotNull
    M remove(@Nullable M model) throws Exception;

    void removeAll(@Nullable List<M> models) throws Exception;

    @NotNull
    M removeById(@Nullable String id) throws Exception;

    @NotNull
    M removeByIndex(@Nullable Integer index) throws Exception;

    @Nullable
    M findOneById(@Nullable String id) throws Exception;

    @Nullable
    M findOneByIndex(@Nullable Integer index) throws Exception;

    @NotNull
    List<M> findAll() throws Exception;

    @NotNull
    List<M> findAll(@Nullable Sort sort) throws Exception;

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator) throws Exception;

    int getSize() throws Exception;

}