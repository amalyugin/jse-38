package ru.t1.malyugin.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.malyugin.tm.api.repository.ITaskRepository;
import ru.t1.malyugin.tm.enumerated.Status;
import ru.t1.malyugin.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return DBConstant.TABLE_TASK;
    }

    @NotNull
    @Override
    public Task fetch(@NotNull final ResultSet row) throws Exception {
        @NotNull final Task task = new Task();
        task.setId(row.getString(DBConstant.COLUMN_ROW_ID));
        task.setProjectId(row.getString(DBConstant.COLUMN_PROJECT_ID));
        task.setName(row.getString(DBConstant.COLUMN_NAME));
        task.setDescription(row.getString(DBConstant.COLUMN_DESCRIPTION));
        task.setStatus(Status.valueOf(row.getString(DBConstant.COLUMN_STATUS)));
        task.setCreated(row.getTimestamp(DBConstant.COLUMN_CREATED));
        task.setUserId(row.getString(DBConstant.COLUMN_USER_ID));
        return task;
    }

    @NotNull
    @Override
    public Task add(@NotNull final Task task) throws Exception {
        @NotNull final String sql = String.format(
                "INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?)",
                getTableName(), DBConstant.COLUMN_ROW_ID, DBConstant.COLUMN_USER_ID, DBConstant.COLUMN_PROJECT_ID,
                DBConstant.COLUMN_CREATED, DBConstant.COLUMN_NAME, DBConstant.COLUMN_DESCRIPTION, DBConstant.COLUMN_STATUS
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getId());
            statement.setString(2, task.getUserId());
            statement.setString(3, task.getProjectId());
            statement.setTimestamp(4, new Timestamp(task.getCreated().getTime()));
            statement.setString(5, task.getName());
            statement.setString(6, task.getDescription());
            statement.setString(7, Status.NOT_STARTED.toString());
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    public Task add(@NotNull final String userId, @NotNull final Task model) throws Exception {
        model.setUserId(userId);
        return add(model);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final List<Task> result = new ArrayList<>();
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? AND %s = ?",
                getTableName(), DBConstant.COLUMN_USER_ID, DBConstant.COLUMN_PROJECT_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) result.add(fetch(resultSet));
        }
        return result;
    }

    @Override
    public void update(@NotNull final Task task) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstant.COLUMN_NAME, DBConstant.COLUMN_DESCRIPTION, DBConstant.COLUMN_STATUS,
                DBConstant.COLUMN_PROJECT_ID, DBConstant.COLUMN_ROW_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getName());
            statement.setString(2, task.getDescription());
            statement.setString(3, task.getStatus().toString());
            statement.setString(4, task.getProjectId());
            statement.setString(5, task.getId());
            statement.executeUpdate();
        }
    }

}