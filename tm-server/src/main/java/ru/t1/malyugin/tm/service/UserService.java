package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.IUserRepository;
import ru.t1.malyugin.tm.api.service.IConnectionService;
import ru.t1.malyugin.tm.api.service.IServiceLocator;
import ru.t1.malyugin.tm.api.service.IUserService;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.exception.field.EmailEmptyException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.LoginEmptyException;
import ru.t1.malyugin.tm.exception.field.PasswordEmptyException;
import ru.t1.malyugin.tm.exception.user.EmailExistException;
import ru.t1.malyugin.tm.exception.user.LoginExistException;
import ru.t1.malyugin.tm.exception.user.UserNotFoundException;
import ru.t1.malyugin.tm.model.User;
import ru.t1.malyugin.tm.repository.UserRepository;
import ru.t1.malyugin.tm.util.HashUtil;

import java.sql.Connection;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IServiceLocator serviceLocator
    ) {
        super(connectionService);
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    protected IUserRepository getRepository(@NotNull final Connection connection) {
        return new UserRepository(connection);
    }

    private void updateUser(@NotNull final User user) throws Exception {
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        try {
            repository.update(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final Role role
    ) throws Exception {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        if (StringUtils.isBlank(password)) throw new PasswordEmptyException();
        if (isLoginExist(login)) throw new LoginExistException();
        if (!StringUtils.isBlank(email) && isEmailExist(email)) throw new EmailExistException();
        @Nullable final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (passwordHash == null) throw new PasswordEmptyException();

        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        if (!StringUtils.isBlank(email)) user.setEmail(email.trim());
        if (role != null) user.setRole(role);
        @NotNull final Connection connection = getConnection();
        @NotNull final IUserRepository repository = getRepository(connection);
        try {
            repository.add(user);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @Override
    public User lockUserByLogin(@Nullable final String login) throws Exception {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        @Nullable final User user = findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        updateUser(user);
        return user;
    }

    @Override
    public User unlockUserByLogin(@Nullable final String login) throws Exception {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        @Nullable final User user = findOneByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        updateUser(user);
        return user;
    }

    @NotNull
    @Override
    public User remove(@Nullable final User user) throws Exception {
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        serviceLocator.getSessionService().clear(userId);
        serviceLocator.getTaskService().clear(userId);
        serviceLocator.getProjectService().clear(userId);
        super.remove(user);
        return user;
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) throws Exception {
        if (StringUtils.isBlank(login)) throw new LoginEmptyException();
        @Nullable final User user = findOneByLogin(login.trim());
        return remove(user);
    }

    @NotNull
    @Override
    public User removeByEmail(@Nullable final String email) throws Exception {
        if (StringUtils.isBlank(email)) throw new EmailEmptyException();
        @Nullable final User user = findOneByEmail(email.trim());
        return remove(user);
    }

    @NotNull
    @Override
    public User setPassword(@Nullable final String id, @Nullable final String password) throws Exception {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        if (StringUtils.isBlank(password)) throw new PasswordEmptyException();
        @Nullable final User user = findOneById(id.trim());
        if (user == null) throw new UserNotFoundException();
        @Nullable final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (passwordHash == null) throw new PasswordEmptyException();
        user.setPasswordHash(passwordHash);
        updateUser(user);
        return user;
    }

    @NotNull
    @Override
    public User updateProfile(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) throws Exception {
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final User user = findOneById(id.trim());
        if (user == null) throw new UserNotFoundException();
        if (!StringUtils.isBlank(firstName)) user.setFirstName(firstName.trim());
        if (!StringUtils.isBlank(lastName)) user.setLastName(lastName.trim());
        if (!StringUtils.isBlank(middleName)) user.setMiddleName(middleName.trim());
        updateUser(user);
        return user;
    }

    @Nullable
    @Override
    public User findOneByLogin(@Nullable final String login) throws Exception {
        if (StringUtils.isBlank(login)) return null;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.findOneByLogin(login.trim());
        }
    }

    @Nullable
    @Override
    public User findOneByEmail(@Nullable final String email) throws Exception {
        if (StringUtils.isBlank(email)) return null;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.findOneByEmail(email.trim());
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExist(@Nullable final String login) throws Exception {
        if (StringUtils.isBlank(login)) return false;
        return findOneByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@Nullable final String email) throws Exception {
        if (StringUtils.isBlank(email)) return false;
        return findOneByEmail(email) != null;
    }

}