package ru.t1.malyugin.tm.service;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.api.repository.IUserOwnedRepository;
import ru.t1.malyugin.tm.api.service.IConnectionService;
import ru.t1.malyugin.tm.api.service.IUserOwnedService;
import ru.t1.malyugin.tm.enumerated.Sort;
import ru.t1.malyugin.tm.exception.entity.EntityNotFoundException;
import ru.t1.malyugin.tm.exception.field.IdEmptyException;
import ru.t1.malyugin.tm.exception.field.IndexIncorrectException;
import ru.t1.malyugin.tm.exception.field.UserIdEmptyException;
import ru.t1.malyugin.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected abstract IUserOwnedRepository<M> getRepository(@NotNull final Connection connection);

    @Nullable
    @Override
    public M add(@Nullable final String userId, final M model) throws Exception {
        if (model == null) return null;
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        @Nullable final Connection connection = getConnection();
        @Nullable final IUserOwnedRepository<M> repository = getRepository(connection);
        try {
            repository.add(userId, model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @Override
    public void clear(@Nullable final String userId) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        @Nullable final Connection connection = getConnection();
        @Nullable final IUserOwnedRepository<M> repository = getRepository(connection);
        try {
            repository.clear(userId);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) throws Exception {
        if (model == null) throw new EntityNotFoundException();
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        @Nullable final Connection connection = getConnection();
        @Nullable final IUserOwnedRepository<M> repository = getRepository(connection);
        try {
            repository.remove(userId, model);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return model;
    }

    @Override
    public void removeAll(@Nullable final String userId, @Nullable final List<M> models) throws Exception {
        if (models == null || models.isEmpty()) return;
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        @Nullable final Connection connection = getConnection();
        @Nullable final IUserOwnedRepository<M> repository = getRepository(connection);
        try {
            repository.removeAll(userId, models);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) throw new IdEmptyException();
        @Nullable final M model = findOneById(userId.trim(), id.trim());
        if (model == null) throw new EntityNotFoundException();
        return remove(userId, model);
    }

    @NotNull
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final M model = findOneByIndex(userId.trim(), index);
        if (model == null) throw new EntityNotFoundException();
        return remove(userId, model);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (StringUtils.isBlank(id)) return null;
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findOneById(userId.trim(), id.trim());
        }
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findOneByIndex(userId.trim(), index);
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findAll(userId.trim());
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.findAll(userId.trim(), comparator);
        }
    }

    @NotNull
    @SuppressWarnings("unchecked")
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @Nullable final Comparator<M> comparator = (Comparator<M>) sort.getComparator();
        return findAll(userId, comparator);
    }

    @Override
    public int getSize(@Nullable final String userId) throws Exception {
        if (StringUtils.isBlank(userId)) throw new UserIdEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserOwnedRepository<M> repository = getRepository(connection);
            return repository.getSize(userId);
        }
    }

}