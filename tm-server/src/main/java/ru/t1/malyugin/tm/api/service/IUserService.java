package ru.t1.malyugin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.enumerated.Role;
import ru.t1.malyugin.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email,
            @Nullable Role role
    ) throws Exception;

    User lockUserByLogin(
            @Nullable String login
    ) throws Exception;

    User unlockUserByLogin(
            @Nullable String login
    ) throws Exception;

    User removeByLogin(
            @Nullable String login
    ) throws Exception;

    @NotNull
    User removeByEmail(
            @Nullable String email
    ) throws Exception;

    @NotNull
    User setPassword(
            @Nullable String id,
            @Nullable String password
    ) throws Exception;

    @NotNull
    User updateProfile(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    ) throws Exception;

    @Nullable
    User findOneByLogin(
            @Nullable String login
    ) throws Exception;

    @Nullable
    User findOneByEmail(
            @Nullable String email
    ) throws Exception;

    @NotNull
    Boolean isLoginExist(
            @Nullable String login
    ) throws Exception;

    @NotNull
    Boolean isEmailExist(
            @Nullable String email
    ) throws Exception;

}