package ru.t1.malyugin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.malyugin.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findOneByLogin(@NotNull String login) throws Exception;

    @Nullable
    User findOneByEmail(@NotNull String email) throws Exception;

    void update(@NotNull User user) throws Exception;

}